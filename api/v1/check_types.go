package v1

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type CheckType string

const (
	HTTP       CheckType = "http"
	HTTPCustom CheckType = "httpcustom"
	TCP        CheckType = "tcp"
	Ping       CheckType = "ping"
	DNS        CheckType = "dns"
	UDP        CheckType = "udp"
	SMTP       CheckType = "smtp"
	POP3       CheckType = "pop3"
	IMAP       CheckType = "imap"
)

// CheckSpec defines the desired state of Check
type CheckSpec struct {
	Name string `json:"name"`
	Host string `json:"host"`
	// +kubebuilder:validation:Enum=http;httpcustom;tcp;ping;dns;udp;smtp;pop3;imap
	Type CheckType `json:"type"`
}

// CheckStatus defines the observed state of Check
type CheckStatus struct {
	ID     *int   `json:"id"`
	Status string `json:"status"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:JSONPath=".status.id",name="ID",type="string"
// +kubebuilder:printcolumn:JSONPath=".spec.type",name="Type",type="string"
// +kubebuilder:printcolumn:JSONPath=".status.status",name="Status",type="string"

// Check is the Schema for the checks API
type Check struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   CheckSpec   `json:"spec,omitempty"`
	Status CheckStatus `json:"status,omitempty"`
}

func (check *Check) String() string {
	return fmt.Sprintf("%v %v ", check.Status.ID, check.Status.Status)
}

// +kubebuilder:object:root=true

// CheckList contains a list of Check
type CheckList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Check `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Check{}, &CheckList{})
}
