package controllers

import (
	pingdomv1 "fmc.pingdom-operator/api/v1"
	"fmt"
	"github.com/russellcardullo/go-pingdom/pingdom"
	"reflect"
)

const (
	finalizer = "delete-check"
)

func (r *CheckReconciler) checkDelete(check *pingdomv1.Check) (bool, error) {
	deleted := check.GetDeletionTimestamp() != nil
	pendingFinalizers := check.GetFinalizers()

	if deleted {
		r.Log.Info("Termination detected")
		if !containsString(pendingFinalizers, finalizer) {
			r.Log.Info("Resource is terminated, skipping reconciliation")
			return false, nil //@toDo should be true? requeue?
		}

		// "Best effort" to delete external resources
		_ = r.deleteExternalResources(check)
		check.SetFinalizers(removeString(pendingFinalizers, finalizer))
		if err := r.updateResource(check); err != nil {
			r.Log.Info("Failed to remove CR uninstall finalizer")
			return true, err
		}
		r.Log.Info("Deleted successfully, finalizer removed, etc")
		return true, nil
	}

	return false, nil
}

func (r *CheckReconciler) registerFinalizer(check *pingdomv1.Check) (bool, error) {
	deleted := check.GetDeletionTimestamp() != nil
	pendingFinalizers := check.GetFinalizers()
	if !deleted && !containsString(pendingFinalizers, finalizer) {
		r.Log.Info("Adding finalizer", "finalizer", finalizer)
		finalizers := append(pendingFinalizers, finalizer)
		check.SetFinalizers(finalizers)
		err := r.updateResource(check)

		// Need to requeue because finalizer update does not change metadata.generation
		return true, err
	}

	return false, nil
}

func (r *CheckReconciler) deleteExternalResources(check *pingdomv1.Check) error {
	pingdomClient, err := New()
	if err != nil {
		return nil
	}

	if check.Status.ID == nil {
		return nil
	}

	id := *check.Status.ID
	if response, err := pingdomClient.Checks.Delete(id); err != nil {
		fmt.Println("Delete response", response)
		fmt.Println("Delete error", err)

	}

	check.Status.Status = ""
	check.Status.ID = nil

	return nil

}
func (r *CheckReconciler) IsSynced(check *pingdomv1.Check) (bool, error) {
	if check.Status.ID == nil {
		return false, nil
	}

	pingdomClient, err := New()
	if err != nil {
		r.Log.Error(err, "Could not connect to api")
		// @todo requeue instead of syncing
		return false, err
	}
	pingdomCheck, err := pingdomClient.Checks.Read(*check.Status.ID)
	if pingdomCheck == nil || err != nil {
		fmt.Println(err)
		fmt.Println(pingdomCheck)
		check.Status.Status = ""
		check.Status.ID = nil
		return false, err
	}

	synced := true
	synced = synced && reflect.DeepEqual(check.Status, pingdomv1.CheckStatus{ID: &pingdomCheck.ID, Status: pingdomCheck.Status})
	synced = synced && check.Name == pingdomCheck.Name
	synced = synced && reflect.DeepEqual(check.Spec, pingdomv1.CheckSpec{Name: pingdomCheck.Name, Host: pingdomCheck.Hostname, Type: pingdomv1.HTTP})

	return synced, nil
}

func (r *CheckReconciler) Sync(check *pingdomv1.Check) error {
	r.Log.Info("Sync check", "check", check)
	pingdomClient, err := New()
	if err != nil {
		r.Log.Error(err, "Could not connect to api")
		return err
	}

	// We dont handle deletion here, only create or update
	// Create it
	var pingdomCheck *pingdom.CheckResponse
	var newCheck pingdom.Check
	if check.Status.ID == nil {
		switch check.Spec.Type {
		case pingdomv1.Ping:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.HTTPCustom:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.HTTP:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.DNS:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.TCP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.UDP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.IMAP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.SMTP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.POP3:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		}
		pingdomCheck, err = pingdomClient.Checks.Create(newCheck)
		if err != nil {
			return err
		}

		check.Status.Status = pingdomCheck.Status
		check.Status.ID = &pingdomCheck.ID
	} else {
		id := check.Status.ID
		switch check.Spec.Type {
		case pingdomv1.Ping:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.HTTPCustom:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.HTTP:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.DNS:
			newCheck = &pingdom.HttpCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.TCP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.UDP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.IMAP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.SMTP:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		case pingdomv1.POP3:
			newCheck = &pingdom.PingCheck{Name: check.Name, Hostname: check.Spec.Host, Resolution: 5}
		}
		if _, err = pingdomClient.Checks.Update(*id, newCheck); err != nil {
			r.Log.Error(err, "Could not update pingdom check", "check", check)
			return err
		}
		pingdomCheck, err := pingdomClient.Checks.Read(*check.Status.ID)
		if err != nil {
			return err
		}
		check.Status.Status = pingdomCheck.Status
	}

	return nil
}

// Helper functions to check and remove string from a slice of strings.
func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func removeString(slice []string, s string) (result []string) {
	for _, item := range slice {
		if item == s {
			continue
		}
		result = append(result, item)
	}
	return
}
