package controllers

import (
	"context"
	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	pingdomv1 "fmc.pingdom-operator/api/v1"
)

// CheckReconciler reconciles a Check object
type CheckReconciler struct {
	client.Client
	Log      logr.Logger
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

// +kubebuilder:rbac:groups=pingdom.fmc.pingdom-operator,resources=checks,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=pingdom.fmc.pingdom-operator,resources=checks/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=pingdom.fmc.pingdom-operator,resources=checks/events,verbs=get;update;patch
// +kubebuilder:rbac:groups=pingdom.fmc.pingdom-operator,resources=events,verbs=get;create;patch

func (r *CheckReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	ctx := context.Background()
	log := r.Log.WithValues("check", req.NamespacedName)

	var check pingdomv1.Check
	if err := r.Get(ctx, req.NamespacedName, &check); err != nil {
		log.Info("Check not found, no resource found")
		r.Recorder.Eventf(&check, "Normal", "SourceNotFound", "Source %s not found, will retry later", check.Name)
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if requeue, _ := r.registerFinalizer(&check); requeue == true {
		r.Recorder.Eventf(&check, "Normal", "AddFinalizer", "Adding finalizer", finalizer)
		return ctrl.Result{RequeueAfter: 30}, nil
	}
	if requeue, err := r.checkDelete(&check); requeue == true {
		if err != nil {
			log.Error(err, "Delete failed", "error", err)
		}
		r.Recorder.Eventf(&check, "Normal", "DeleteResource", "Delete resource", check)
		return ctrl.Result{RequeueAfter: 60}, nil
	}

	if synced, _ := r.IsSynced(&check); synced != true {
		if err := r.Sync(&check); err != nil {
			log.Error(err, "Could not Sync", "err", err)
			r.Recorder.Eventf(&check, "Normal", "SyncFailed", "Sync tried and failed with error ", err)
			return ctrl.Result{Requeue: true}, err
		}

		err := r.Status().Update(ctx, &check)
		if err != nil {
			log.Error(err, "Could not update status", "err", err)
			r.Recorder.Eventf(&check, "Normal", "SyncFailed", "Sync tried and failed with error ", err)
			return ctrl.Result{Requeue: true}, err
		}
		r.Recorder.Eventf(&check, "Normal", "SyncCompleted", "Sync completed ", check)
	}

	// update status, events
	// update transactions

	return ctrl.Result{RequeueAfter: 30}, nil
}

func (r *CheckReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&pingdomv1.Check{}).
		Complete(r)
}

//func (r *CheckReconciler) updateResource(check *pingdomv1.Check) error {
func (r *CheckReconciler) updateResource(o runtime.Object) error {
	return r.Client.Update(context.Background(), o)
}
